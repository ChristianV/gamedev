﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    public float runSpeed;
    public float walkSpeed;
    Rigidbody RB;
    Animator Anim;

    bool facingRight;

    bool grounded = false;
    Collider[] groundCollisions;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

	// Use this for initialization
	void Start () {
        RB = GetComponent<Rigidbody>();
        Anim = GetComponent<Animator>();
        facingRight = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        if(grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            Anim.SetBool("grounded", false);
            RB.AddForce(new Vector3(0, jumpHeight, 0));
        }

        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
        if (groundCollisions.Length > 0) grounded = true;
        else grounded = false;

        Anim.SetBool("grounded", grounded);



        float move = Input.GetAxis("Horizontal");
        Anim.SetFloat("speed", Mathf.Abs(move));

        float sneaking = Input.GetAxisRaw("Fire3");
        Anim.SetFloat("sneaking", sneaking);

        if(sneaking>0 && grounded)
            RB.velocity = new Vector3(move * walkSpeed, RB.velocity.y, 0);
        else
            RB.velocity = new Vector3(move * runSpeed, RB.velocity.y, 0);

        if (move > 0 && !facingRight) Flip();
        else if (move < 0 && facingRight) Flip();
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
    }
}
